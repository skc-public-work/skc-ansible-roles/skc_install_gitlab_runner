Role Name
=========

This role will install the GitLab runner software

Requirements
------------

debian system

Role Variables
--------------



Dependencies
------------



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - role: skc_install_gitlab_runner

License
-------

BSD

Author Information
------------------

